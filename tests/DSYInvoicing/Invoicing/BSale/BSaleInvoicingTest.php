<?php 

namespace Tests\DSYInvoicing\Invoicing\BSale;

use Tests\BaseTestCase;
use DSYInvoicing\Invoicing\InvoicingResolver;
use DSYInvoicing\Invoicing\BSale\Options\ResolveBSaleOptions;
use DSYInvoicing\Invoicing\Options;

class BSaleInvoicingTest extends BaseTestCase
{
    
    private $baseDocumentOptions = [
        'documentTypeId' => 5,
        'clientRut' => '76233728-2',
        'clientActivity' => 'Software',
        'clientName' => 'dsarhoya',
        'clientCity' => 'Santiago',
        'clientAddress' => 'Kennedy 7534',
        'clientMunicipality' => 'Las Condes',
    ];
    
    private $firstBaseDetail = [
        'netUnitValue' => 10000,
        'quantity' => 2,
        'comment' => 'Licencias de software',
        'discount' => 12.5,
    ];
    
    private $secondBaseDetail = [
        'netUnitValue' => 2000000,
        'quantity' => 1,
        'comment' => 'Desarrollo de software',
    ];
    
    /**
     * @expectedException \DSYInvoicing\Exception\DSYInvoicingException
     */
    public function testItCantCreateIncomeInvoiceWithTaxWithSiiCode(){
        
        $invoicingResolver = new InvoicingResolver();
        $bSale = $invoicingResolver->resolveBSaleInvoicing(new ResolveBSaleOptions([
            'accessToken' => 'aToken'
        ]));
        
        $expiration = new \Datetime();
        $expiration->add(new \DateInterval('P1M'));
        $options = $this->baseDocumentOptions;
        $options['expirationDateTimestamp'] = $expiration->getTimestamp();
        $options['documentTypeId'] = 5;
        $options['documentSiiCode'] = 32;
        $options['details'] = [
            new Options\CreateDocumentDetailOptions($this->firstBaseDetail),
            new Options\CreateDocumentDetailOptions($this->secondBaseDetail),
        ];

        $bSale->createInvoiceWithTax(new Options\CreateInvoiceWithTaxOptions($options));
    }
}
