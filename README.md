dsy-invoices-bundle
================

Importante, dado que este es un proyecto de desarrollo, el archivo composer.json se ocupa para definici�n del paquete, no para la importaci�n de dependencias.
Para eso est� el archivo composer-dev.json, el cual se tiene que usar de la siguiente manera:
```
COMPOSER=composer-dev.json composer install
```
De esta manera, composer sabe que las dependencias de este proyecto est�n en otro archivo.

C�digos SII
================

http://www.sii.cl/declaraciones_juradas/ddjj_3327_3328/instrucciones_llenado_comp_vtas.pdf