<?php 

namespace dsarhoya\DSYInvoiceBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * 
 */
class GenerateInvoicesEvent extends Event
{
    /**
     * @var array
     */
    private $invoices;

    /**
     * Get the value of Invoices 
     * 
     * @return array
     */
    public function getInvoices()
    {
        return $this->invoices;
    }
 
    /** 
     * Set the value of Invoices 
     * 
     * @param array invoices
     * 
     * @return self
     */
    public function setInvoices(array $invoices)
    {
        $this->invoices = $invoices;
 
        return $this;
    }
 
}
