<?php

namespace dsarhoya\DSYInvoiceBundle\Event;

/**
 * Description of InvoicesBundleEvents
 *
 * @author mati <matias.castro@dsarhoya.cl>
 */
final class InvoiceBundleEvents {
    const GENERATE_INVOICES = 'invoice_bundle.generate_invoices';
}
