<?php

namespace dsarhoya\DSYInvoiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminInvoiceClientType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('legalName')
            ->add('legalId')
            ->add('submit', SubmitType::class, array(
                'label'=>'Guardar',
                'attr'=>array('class'=>'btn btn-success')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'dsarhoya\DSYInvoiceBundle\Entity\AdminInvoiceClient'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dsarhoya_dsyinvoicebundle_admininvoiceclient';
    }
}
