<?php

namespace dsarhoya\DSYInvoiceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminInvoiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', null, array(
                'label'=>'Fecha',
                'widget'=>'single_text',
                'format'=>'dd-MM-yyyy',
                'attr'=>array('class'=>'datepicker')
            ))
            ->add('client', EntityType::class, array(
                'label'=>'Cliente',
                'class'=>'dsarhoyaDSYInvoiceBundle:AdminInvoiceClient',
                'choice_label'=>'legalName',
                'choices'=>$options['clients']
            ))
            ->add('amount',null, array(
                'label'=>'Cantidad'
            ))
            ->add('currency', ChoiceType::class, array(
                'label'=>'Moneda',
                'choices'=>  \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice::currenciesArray(),
                'multiple'=>false,
                'expanded'=>false
            ))
            ->add('comment',null, array(
                'label'=>'Comentario'
            ))
            ->add('serviceName',null, array(
                'label'=>'Nombre del servicio'
            ))
            ->add('submit', SubmitType::class, array(
                'label'=>'Guardar',
                'attr'=>array('class'=>'btn btn-success')
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice',
            'clients'=> []
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dsarhoya_dsyinvoicebundle_admininvoice';
    }
}
