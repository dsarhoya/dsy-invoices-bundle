<?php

namespace dsarhoya\DSYInvoiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use dsarhoya\DSYInvoiceBundle\Event\InvoiceBundleEvents;
use dsarhoya\DSYInvoiceBundle\Event\GenerateInvoicesEvent;

class InvoiceController extends Controller
{
    public function getInvoicesAction(Request $request){
        $invoices = [];
        $date = new \DateTime('now');
        if($request->get('date', null)){
            $date = \DateTime::createFromFormat('Y-m-d', $request->get('date'));
        }
        
        $event = $this->get('event_dispatcher')->dispatch(InvoiceBundleEvents::GENERATE_INVOICES, new GenerateInvoicesEvent());
        if (null !== $generatedInvoices = $event->getInvoices()) {
            foreach ($generatedInvoices as $generatedInvoice) {
                if(!is_a($generatedInvoice, 'dsarhoya\DSYInvoiceBundle\Entity\Invoice')) throw new \Exception('Inoices must be dsarhoya\DSYInvoiceBundle\Entity\Invoice');
                $invoices[] = $generatedInvoice;
            }
        }
        
        //TODO: devolver las invoices para la fecha.
        $adminInvoices = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoice')->invoicesForMonth($date);
        
        /* @var $adminInvoice \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice */
        foreach ($adminInvoices as $adminInvoice){
            $inv = new \dsarhoya\DSYInvoiceBundle\Model\Invoice();
            $inv->setClientLegalId($adminInvoice->getClient()->getLegalId());
            $inv->setClientName($adminInvoice->getClient()->getLegalName());
            $inv->setValue($adminInvoice->getAmount());
            $inv->setCurrency($adminInvoice->getCurrency());
            $inv->setComment($adminInvoice->getComment());
            $inv->setServiceName($adminInvoice->getServiceName() ? $adminInvoice->getServiceName() : 'Sin nombre de servicio');
            $invoices[] = $inv;
        }
        
        return $this->returnJson(array_map(function($invoice){
            return $invoice->getJSON();
        }, $invoices));
        
    }
    
    private function returnJson($array){
        $response = new Response(json_encode($array));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    public function getAdminInvoicesAction(Request $request, $invoiceId){
        
        if(is_null($invoiceId)){
            $invoice = new \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice();
            $invoice->setServiceName($this->container->hasParameter('dsarhoya_base.serviceName') ? $this->container->getParameter('dsarhoya_base.serviceName') : null);
        }else{
            $invoice = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoice')->find($invoiceId);
        }
        
        $clients = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoiceClient')->findAll();
        $form = $this->createForm(\dsarhoya\DSYInvoiceBundle\Form\AdminInvoiceType::class, $invoice, array(
            'clients'=> $clients
        ));
        
        if($request->getMethod()=="POST" && $form->handleRequest($request)->isValid()){
            if(is_null($invoice->getId())){
                $this->getDoctrine()->getManager()->persist($invoice);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('dsy_extension_get_invoices'));
        }
        
        $invoices = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoice')->findBy(array(), array('date'=>'DESC'));
        
        return $this->render('dsarhoyaDSYInvoiceBundle:Admin:all.html.twig', array(
            'form'=>$form->createView(),
            'clients'=>$clients,
            'invoices'=>$invoices
        ));
    }
    
    public function getAdminClientsAction(Request $request, $clientId){
        
        if(is_null($clientId)){
            $client = new \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoiceClient();
        }else{
            $client = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoiceClient')->find($clientId);
        }
        
        $form = $this->createForm(\dsarhoya\DSYInvoiceBundle\Form\AdminInvoiceClientType::class, $client);
        
        if($request->getMethod()=="POST"&&$form->handleRequest($request)->isValid()){
            if(is_null($client->getId())) $this->getDoctrine()->getManager()->persist($client);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('dsy_extension_get_clients'));
        }
        
        $clients = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoiceClient')->findAll();
        
        return $this->render('dsarhoyaDSYInvoiceBundle:Admin:clients.html.twig', array(
            'form'=>$form->createView(),
            'clients'=>$clients
        ));
    }
    
    public function removeAdminClientAction($clientId){
        $client = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoiceClient')->find($clientId);
        $this->getDoctrine()->getManager()->remove($client);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('dsy_extension_get_clients'));
    }
    
    public function removeAdminInvoiceAction($invoiceId){
        $invoice = $this->getDoctrine()->getRepository('dsarhoyaDSYInvoiceBundle:AdminInvoice')->find($invoiceId);
        $this->getDoctrine()->getManager()->remove($invoice);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirect($this->generateUrl('dsy_extension_get_invoices'));
    }
}
