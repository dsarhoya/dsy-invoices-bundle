<?php

namespace dsarhoya\DSYInvoiceBundle\Model;

/**
 * Description of Invoice
 *
 * @author matias
 */
class Invoice {
    
    const CURRENCY_CHILEAN_PESOS = 'clp';
    const CURRENCY_UF = 'UF';
    
    private $service_name;
    private $value;
    private $currency;
    private $comment;
    private $client_name;
    private $client_legal_id;
    
    public function setServiceName($service_name){
        $this->service_name = $service_name;
        return $this;
    }
    
    public function setValue($value){
        $this->value    = $value;
        return $this;
    }
    
    public function setCurrency($currency){
        if(!in_array($currency, self::currencies())) throw new \Exception('Unkonw currencie');
        $this->currency = $currency;
        return $this;
    }
    
    public function setComment($comment){
        $this->comment  = $comment;
        return $this;
    }
    
    public function setClientName($client_name){
        $this->client_name  = $client_name;
        return $this;
    }
    
    public function setClientLegalId($client_legal_id){
        $this->client_legal_id = $client_legal_id;
        return $this;
    }
    
    public function getJSON(){
        
        if(!$this->service_name) throw new \Exception('An invoice must have a service name');
        if(!$this->value) throw new \Exception('An invoice must have a value');
        if(!$this->comment) throw new \Exception('An invoice must have a comment');
        if(!$this->currency) throw new \Exception('An invoice must have a currency');
        if(!$this->client_name) throw new \Exception('An invoice must have the client\'s name');
        if(!$this->client_legal_id) throw new \Exception('An invoice must have the client\'s legal id');
        
        $array = array();
        $array['service_name']=$this->service_name;
        $array['value']=$this->value;
        $array['comment']=$this->comment;
        $array['client_name']=$this->client_name;
        $array['client_legal_id']=$this->client_legal_id;
        $array['currency']=$this->currency;
        
        return $array;
        
    }
    
    public static function currencies(){
        $currencies = array();
        $currencies[] = self::CURRENCY_CHILEAN_PESOS;
        $currencies[] = self::CURRENCY_UF;
        return $currencies;
    }
}