<?php

namespace dsarhoya\DSYInvoiceBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of AdminInvoiceRepository
 *
 * @author matias
 */
class AdminInvoiceRepository extends EntityRepository
{
    public function invoicesForMonth(\DateTime $date){
        
        $date_from = clone $date;
        $date_from = \DateTime::createFromFormat('d-m-Y H:i:s', sprintf('01-%s-%s 00:00:00', $date_from->format('m'), $date_from->format('Y')));
        $date_to = clone $date_from;
        $date_to = $date_to->add(new \DateInterval('P1M'));
        $date_to = $date_to->sub(new \DateInterval('P1D'));
        
        $qb = $this->createQueryBuilder('i');
        $qb->add('select', 'i');
        $qb->add('from', 'dsarhoyaDSYInvoiceBundle:AdminInvoice i');
        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->gte('i.date', ':date_from'));
        $andX->add($qb->expr()->lte('i.date', ':date_to'));
        $qb->add('where', $andX);
        $qb->setParameters(array(
            'date_from'=>$date_from,
            'date_to'=>$date_to
        ));
        
        return $qb->getQuery()->getResult();
    }
}