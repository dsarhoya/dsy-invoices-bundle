<?php

namespace dsarhoya\DSYInvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminInvoiceClient
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class AdminInvoiceClient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var type 
     * 
     * @ORM\Column(type="string", nullable=false)
     */
    private $legalName;

    /**
     * @var type 
     * 
     * @ORM\Column(type="string", nullable=false)
     */
    private $legalId;
    
    /**
     * @ORM\OneToMany(targetEntity="AdminInvoice", mappedBy="client")
     */
    private $invoices;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set legalName
     *
     * @param string $legalName
     * @return AdminInvoiceClient
     */
    public function setLegalName($legalName)
    {
        $this->legalName = $legalName;

        return $this;
    }

    /**
     * Get legalName
     *
     * @return string 
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * Set legalId
     *
     * @param string $legalId
     * @return AdminInvoiceClient
     */
    public function setLegalId($legalId)
    {
        $this->legalId = $legalId;

        return $this;
    }

    /**
     * Get legalId
     *
     * @return string 
     */
    public function getLegalId()
    {
        return $this->legalId;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->invoices = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add invoices
     *
     * @param \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice $invoices
     * @return AdminInvoiceClient
     */
    public function addInvoice(\dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice $invoices)
    {
        $invoices->setClient($this);
        $this->invoices[] = $invoices;

        return $this;
    }

    /**
     * Remove invoices
     *
     * @param \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice $invoices
     */
    public function removeInvoice(\dsarhoya\DSYInvoiceBundle\Entity\AdminInvoice $invoices)
    {
        $this->invoices->removeElement($invoices);
    }

    /**
     * Get invoices
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvoices()
    {
        return $this->invoices;
    }
}
