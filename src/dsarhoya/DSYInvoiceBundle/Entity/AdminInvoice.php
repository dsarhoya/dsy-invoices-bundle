<?php

namespace dsarhoya\DSYInvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminInvoice
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="dsarhoya\DSYInvoiceBundle\Repository\AdminInvoiceRepository")
 */
class AdminInvoice
{
    CONST CURRENCY_PESOS = 'clp';
    CONST CURRENCY_UF = 'UF';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @var type 
     * 
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date;
    
    /**
     *
     * @var type 
     * 
     * @ORM\Column(type="float", nullable=false)
     */
    private $amount;
    
    /**
     *
     * @var type 
     * 
     * @ORM\Column(type="string", nullable=false)
     */
    private $currency;
    
    /**
     *
     * @var type 
     * 
     * @ORM\Column(type="string", nullable=false)
     */
    private $comment;
    
    /**
     *
     * @var type 
     * 
     * @ORM\Column(type="string", nullable=false)
     */
    private $serviceName;

    /**
     * @ORM\ManyToOne(targetEntity="AdminInvoiceClient", inversedBy="invoices")
     * @ORM\JoinColumn(name="adminInvoiceClient_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $client;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return AdminInvoice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set client
     *
     * @param \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoiceClient $client
     * @return AdminInvoice
     */
    public function setClient(\dsarhoya\DSYInvoiceBundle\Entity\AdminInvoiceClient $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \dsarhoya\DSYInvoiceBundle\Entity\AdminInvoiceClient 
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return AdminInvoice
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return AdminInvoice
     */
    public function setCurrency($currency)
    {
        if(!in_array($currency, self::currenciesArray())) throw new \Exception('Moneda no reconocida');
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    
    public static function currenciesArray(){
        $currencies = array();
        $currencies[self::CURRENCY_UF] = self::CURRENCY_UF;
        $currencies[self::CURRENCY_PESOS] = self::CURRENCY_PESOS;
        return $currencies;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return AdminInvoice
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     * @return AdminInvoice
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string 
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }
}
