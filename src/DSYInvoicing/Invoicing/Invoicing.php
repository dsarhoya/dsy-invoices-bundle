<?php 

namespace DSYInvoicing\Invoicing;

use DSYInvoicing\Invoicing\Options;

/**
 * Invoicing
 */
abstract class Invoicing
{
    abstract public function createInvoiceWithTax(Options\CreateInvoiceWithTaxOptions $options);
    abstract public function createInvoiceWithoutTax(Options\CreateInvoiceWithoutTaxOptions $options);
    abstract public function createInvoice(Options\CreateDocumentOptions $options);
    abstract public function getDocuments(Options\GetDocumentsOptions $options);
    abstract public function getDocumentTypes(Options\GetDocumentTypesOptions $options);
    
}
