<?php 

namespace DSYInvoicing\Invoicing\Options;

use DSYInvoicing\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateDocumentDetailOptions
 */
class CreateDocumentDetailOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'discount' => null
        ]);
        
        $resolver->setRequired('netUnitValue')->setAllowedTypes('netUnitValue', ['int']);
        $resolver->setRequired('quantity')->setAllowedTypes('quantity', ['int']);
        $resolver->setRequired('comment')->setAllowedTypes('comment', ['string']);
        $resolver->setAllowedTypes('discount', ['null', 'float']);
    }
}
