<?php 

namespace DSYInvoicing\Invoicing\Options;

use DSYInvoicing\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;
use DSYInvoicing\Invoicing\Options\CreateDocumentDetailOptions;

/**
 * CreateDocumentOptions
 */
class CreateDocumentOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'documentTypeId' => null,
            'documentSiiCode' => null,
            'emissionDateTimestamp' => null,
            'clientName' => null,
            'clientCity' => null,
            'clientAddress' => null,
            'clientMunicipality' => null,
        ]);
        
        $resolver->setRequired('details')->setAllowedTypes('details', ['array'])->setAllowedValues('details', function($value){
            foreach ($value as $item) {
                if(!($item instanceof CreateDocumentDetailOptions)){
                    return false;
                }
            }
            return true;
        });
        $resolver->setAllowedTypes('documentTypeId', ['null', 'int']);
        $resolver->setAllowedTypes('documentSiiCode', ['null', 'int']);
        $resolver->setAllowedTypes('emissionDateTimestamp', ['null', 'int']);
        $resolver->setRequired('expirationDateTimestamp')->setAllowedTypes('expirationDateTimestamp', ['int']);
        $resolver->setRequired('clientRut')->setAllowedTypes('clientRut', ['string']);
        $resolver->setRequired('clientActivity')->setAllowedTypes('clientActivity', ['string']);
        $resolver->setAllowedTypes('clientName', ['null', 'string']);
        $resolver->setAllowedTypes('clientCity', ['null', 'string']);
        $resolver->setAllowedTypes('clientAddress', ['null', 'string']);
        $resolver->setAllowedTypes('clientMunicipality', ['null', 'string']);
    }
}
