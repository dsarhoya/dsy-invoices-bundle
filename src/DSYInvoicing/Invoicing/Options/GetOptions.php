<?php 

namespace DSYInvoicing\Invoicing\Options;

use DSYInvoicing\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * GetOptions
 */
class GetOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'recursive' => null,
            'offset' => 0,
            'limit' => 25,
        ]);
        
        $resolver->setAllowedTypes('recursive', ['null', 'boolean']);
        $resolver->setAllowedTypes('offset', ['int']);
        $resolver->setAllowedTypes('limit', ['int']);
    }
}
