<?php 

namespace DSYInvoicing\Invoicing\Options;

use DSYInvoicing\Invoicing\Options\CreateDocumentOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateInvoiceWithoutTaxOptions
 */
class CreateInvoiceWithoutTaxOptions extends CreateDocumentOptions
{

}
