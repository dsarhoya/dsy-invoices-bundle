<?php 

namespace DSYInvoicing\Invoicing\Test;

use DSYInvoicing\Invoicing\Invoicing;
use DSYInvoicing\Invoicing\Options\CreateInvoiceWithTaxOptions;
use DSYInvoicing\Model\Document;

use DSYInvoicing\BaseOptions;
use DSYInvoicing\Invoicing\Options as DSYInvoicingOptions;
/**
 * TestInvoicing
 */
class TestInvoicing extends Invoicing
{
    public function createInvoiceWithTax(CreateInvoiceWithTaxOptions $options){
        $invoice = new Document();
        $invoice->setNumber(1);
        $invoice->setFileUrl('https://dsy.cl');
        return $invoice;
    }
    
    public function createInvoiceWithoutTax(DSYInvoicingOptions\CreateInvoiceWithoutTaxOptions $options){
        
    }
    
    public function createInvoice(DSYInvoicingOptions\CreateDocumentOptions $options){
        
    }
    
    public function getDocuments(DSYInvoicingOptions\GetDocumentsOptions $options){
        
    }
    
    public function getDocumentTypes(DSYInvoicingOptions\GetDocumentTypesOptions $options){
        
    }
}
