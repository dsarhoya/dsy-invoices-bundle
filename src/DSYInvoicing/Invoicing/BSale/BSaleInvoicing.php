<?php 

namespace DSYInvoicing\Invoicing\BSale;

use DSYInvoicing\Invoicing\Invoicing;
use DSYInvoicing\Model\Document;
use DSYInvoicing\Model\DocumentType;
use DSYBSaleClient\Client;

use DSYInvoicing\BaseOptions;
use DSYInvoicing\Invoicing\Options as DSYInvoicingOptions;
use DSYInvoicing\Invoicing\BSale\Options\ResolveBSaleOptions;
use DSYInvoicing\Exception\DSYInvoicingException;
use DSYInvoicing\SiiDocumentCodes;

//Esto es del cliente
use DSYBSaleClient\Elements\Options\Documents as DocumentsOptions;
use DSYBSaleClient\Elements\Options\DocumentTypes as DocumentTypesOptions;
use DSYBSaleClient\Options\CreateClientOptions;

/**
 * BSaleInvoicing
 *
 * Acá sí "conozco" el cliente de bsale por lo que lo puedo usar sin problemas.
 * 
 */
class BSaleInvoicing extends Invoicing
{
    /**
     * @var Client
     */
    private $bSaleClient;
    
    public function __construct(ResolveBSaleOptions $options){
        $this->bSaleClient = new Client(new CreateClientOptions([
            'accessToken' => $options->accessToken
        ]));
    }
    
    /**
     * La idea de diferenciar estos dos métodos es el uso o no del parámetro documentTypeId. En Bsale se puede 
     * usar ese parámetro o se puede usar directamente el código de documento del SII. Es probable que otros integradores
     * no ocupen el tipo de documento y sí el código SII. En ese caso, hay 
     * 
     * @param  DSYInvoicingOptionsCreateInvoiceWithTaxOptions $options [description]
     * @return [type]                                                  [description]
     */
    public function createInvoiceWithTax(DSYInvoicingOptions\CreateInvoiceWithTaxOptions $options){
        if (null !== $options->documentTypeId || null !== $options->documentSiiCode) {
            throw new DSYInvoicingException('When creating invoice with tax, no document type nor code info should be set');
        }
        $originalOptions = $options->getOriginalInput();
        $originalOptions['documentSiiCode'] = SiiDocumentCodes::FACTURA_ELECTRONICA_AFECTA;
        return $this->createInvoice(new DSYInvoicingOptions\CreateInvoiceWithTaxOptions($originalOptions));
    }
    
    public function createInvoiceWithoutTax(DSYInvoicingOptions\CreateInvoiceWithoutTaxOptions $options){
        if (null !== $options->documentTypeId || null !== $options->documentSiiCode) {
            throw new DSYInvoicingException('When creating invoice without tax, no document type nor code info should be set');
        }
        $originalOptions = $options->getOriginalInput();
        $originalOptions['documentSiiCode'] = SiiDocumentCodes::FACTURA_ELECTRONICA_NO_AFECTA;
        return $this->createInvoice($options);
    }
    
    public function createInvoice(DSYInvoicingOptions\CreateDocumentOptions $options){
        $originalOptions = $options->getOriginalInput();
        $originalOptions['details'] = array_map(function($detailOptions){
            $detail = [
                'netUnitValue' => $detailOptions->netUnitValue,
                'quantity' => $detailOptions->quantity,
                'comment' => $detailOptions->comment,
            ];
            if (null !== $detailOptions->discount) {
                $detail['discount'] = $detailOptions->discount;
            }
            return new DocumentsOptions\CreateDocumentDetailOptions($detail);
        }, $options->details);
        $res = $this->bSaleClient->documents->createDocument(new DocumentsOptions\CreateDocumentOptions($originalOptions));
        $invoice = new Document();
        $invoice->setNumber($res['number']);
        $invoice->setFileUrl($res['urlPdf']);
        $invoice->setOriginalDataArray($res);
        return $invoice;
    }
    
    private function processGetOptions(BaseOptions $options){
        $transformedOptions = [
            'recursive' => null === $options->recursive ? false : true,
        ];
        if (null !== $options->limit) {
            $transformedOptions['limit'] = $options->limit;
        }
        if (null !== $options->offset) {
            $transformedOptions['offset'] = $options->offset;
        }
        return $transformedOptions;
    }
    
    private function processDocument($element){
        $invoice = new Document();
        $invoice->setNumber($element['number']);
        $invoice->setFileUrl($element['urlPdf']);
        $invoice->setOriginalDataArray($element);
        return $invoice;
    }
    
    public function getDocuments(DSYInvoicingOptions\GetDocumentsOptions $options){
        $bSaleOptions = $this->processGetOptions($options);
        $res = $this->bSaleClient->documents->getDocuments(new DocumentsOptions\GetDocumentsOptions($bSaleOptions));
        $documents = array_map([$this, 'processDocument'], $res);
        return $documents;
    }
    
    public function getDocumentTypes(DSYInvoicingOptions\GetDocumentTypesOptions $options){
        $bSaleOptions = $this->processGetOptions($options);
        $res = $this->bSaleClient->documentTypes->getDocumentTypes(new DocumentTypesOptions\GetDocumentTypesOptions($bSaleOptions));
        $types = array_map(function($type){
            $docType = new DocumentType();
            $docType->setName($type['name']);
            $docType->setId($type['id']);
            $docType->setOriginalDataArray($type);
            return $docType;
        }, $res);
        return $types;
    }
}
