<?php 

namespace DSYInvoicing\Invoicing\BSale\Options;

use DSYInvoicing\BaseOptions;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CreateInvoiceWithTaxOptions
 */
class ResolveBSaleOptions extends BaseOptions
{
    public function configureOptions(OptionsResolver $resolver){
        // $resolver->setDefaults(array(
        //     'accessToken' => null,
        // ));
        
        $resolver->setRequired('accessToken')->setAllowedTypes('accessToken', array('string'));
    }
}
