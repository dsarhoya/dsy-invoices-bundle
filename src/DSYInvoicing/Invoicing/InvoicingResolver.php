<?php 

namespace DSYInvoicing\Invoicing;

use DSYInvoicing\Invoicing\Test\TestInvoicing;
use DSYInvoicing\Invoicing\BSale\BSaleInvoicing;
use DSYInvoicing\Invoicing\BSale\Options\ResolveBSaleOptions;

/**
 * InvoicingResolver
 */
class InvoicingResolver
{
    public function resolveTestInvoicing(){
        return new TestInvoicing(); //acá tendría que pasarle todo lo que necesita para hacer la llamada.
    }
    
    public function resolveBSaleInvoicing(ResolveBSaleOptions $options){
        return new BSaleInvoicing($options);
    }
}
