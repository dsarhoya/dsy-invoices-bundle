<?php

namespace DSYInvoicing;

/**
 * Description of SiiDocumentCodes
 *
 * @author matias
 */
class SiiDocumentCodes{
    CONST FACTURA_ELECTRONICA_AFECTA = 33;
    CONST FACTURA_ELECTRONICA_NO_AFECTA = 33;
    CONST NOTA_DE_CREDITO = 61;
}
