<?php 

namespace DSYInvoicing\Model;

/**
 * Document
 */
class Document
{
    /**
     * @var int
     */
    protected $number;
    
    /**
     * @var string
     */
    protected $fileUrl;
    
    /**
     * @var array
     */
    protected $originalDataArray;
    
    /**
     * Get the value of Number 
     * 
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }
 
    /** 
     * Set the value of Number 
     * 
     * @param int number
     * 
     * @return self
     */
    public function setNumber($number)
    {
        $this->number = $number;
 
        return $this;
    }
 
    /**
     * Get the value of File Url 
     * 
     * @return string
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }
 
    /** 
     * Set the value of File Url 
     * 
     * @param string fileUrl
     * 
     * @return self
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;
 
        return $this;
    }
 

    /**
     * Get the value of Original Data Array 
     * 
     * @return array
     */
    public function getOriginalDataArray()
    {
        return $this->originalDataArray;
    }
 
    /** 
     * Set the value of Original Data Array 
     * 
     * @param array originalDataArray
     * 
     * @return self
     */
    public function setOriginalDataArray(array $originalDataArray)
    {
        $this->originalDataArray = $originalDataArray;
 
        return $this;
    }
 
}
