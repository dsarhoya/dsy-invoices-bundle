<?php 

namespace DSYInvoicing\Model;

/**
 * DocumentType
 */
class DocumentType
{
    /**
     * @var int
     */
    protected $id;
    
    /**
     * @var string
     */
    protected $name;
    
    /**
     * @var array
     */
    protected $originalDataArray;
    
    /**
     * Get the value of Id 
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
 
    /** 
     * Set the value of Id 
     * 
     * @param int id
     * 
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;
 
        return $this;
    }
 
    /**
     * Get the value of Name 
     * 
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
 
    /** 
     * Set the value of Name 
     * 
     * @param string name
     * 
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
 
        return $this;
    }
 
    /**
     * Get the value of Original Data Array 
     * 
     * @return array
     */
    public function getOriginalDataArray()
    {
        return $this->originalDataArray;
    }
 
    /** 
     * Set the value of Original Data Array 
     * 
     * @param array originalDataArray
     * 
     * @return self
     */
    public function setOriginalDataArray(array $originalDataArray)
    {
        $this->originalDataArray = $originalDataArray;
 
        return $this;
    }
 
}
