<?php 

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use dsarhoya\BaseBundle\Entity\BaseUser;
use dsarhoya\BaseBundle\Entity\BaseUserInterface;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser implements BaseUserInterface
{
    /**
     * @ORM\OneToMany(targetEntity="dsarhoya\BaseBundle\Entity\UserKey", mappedBy="user")
     */
    private $keys;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="Profile", inversedBy="users")
     * @ORM\JoinColumn(name="profile_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $profile;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->keys = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add key.
     *
     * @param \dsarhoya\BaseBundle\Entity\UserKey $key
     *
     * @return User
     */
    public function addKey(\dsarhoya\BaseBundle\Entity\UserKey $key)
    {
        $this->keys[] = $key;

        return $this;
    }

    /**
     * Remove key.
     *
     * @param \dsarhoya\BaseBundle\Entity\UserKey $key
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeKey(\dsarhoya\BaseBundle\Entity\UserKey $key)
    {
        return $this->keys->removeElement($key);
    }

    /**
     * Get keys.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeys()
    {
        return $this->keys;
    }

    /**
     * Set company.
     *
     * @param \AppBundle\Entity\Company|null $company
     *
     * @return User
     */
    public function setCompany(\AppBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \AppBundle\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set profile.
     *
     * @param \AppBundle\Entity\Profile|null $profile
     *
     * @return User
     */
    public function setProfile(\AppBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }
}
