<?php

namespace AppBundle\Repository;

use dsarhoya\BaseBundle\Entity\BaseUserRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * 
 */
class UserRepository extends BaseUserRepository implements UserLoaderInterface
{
}
