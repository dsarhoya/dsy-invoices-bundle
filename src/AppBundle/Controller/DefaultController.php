<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use DSYInvoicing\Invoicing\InvoicingResolver;
use DSYInvoicing\Invoicing\BSale\Options\ResolveBSaleOptions;
use DSYInvoicing\Invoicing\Options;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        
        $invoicingResolver = new InvoicingResolver();
        $factory = $invoicingResolver->resolveTestInvoicing();
        $invoicing = $invoicingResolver->resolveBSaleInvoicing(new ResolveBSaleOptions([
            'accessToken' => 'eab9450ae40cd8cba38a7f7ab6f850435155673a'
        ]));
        dump($types = $invoicing->getDocumentTypes(new Options\GetDocumentTypesOptions([
            'limit' => 10,
        ])));
        // die;
        $expiration = new \Datetime();
        $expiration->add(new \DateInterval('P1M'));
        dump($invoicing->createInvoiceWithTax(new Options\CreateInvoiceWithTaxOptions([
            //'documentTypeId' => 5,
            'expirationDateTimestamp' => $expiration->getTimestamp(),
            'clientRut' => '76233728-2',
            'clientActivity' => 'Software',
            'clientName' => 'dsarhoya',
            'clientCity' => 'Santiago',
            'clientAddress' => 'Kennedy 7534',
            'clientMunicipality' => 'Las Condes',
            'details' => [
                new Options\CreateDocumentDetailOptions([
                    'netUnitValue' => 10000,
                    'quantity' => 2,
                    'comment' => 'Licencias de software',
                    'discount' => 12.5,
                ]),
                new Options\CreateDocumentDetailOptions([
                    'netUnitValue' => 2000000,
                    'quantity' => 1,
                    'comment' => 'Desarrollo de software',
                    'discount' => (float)15,
                ]),
            ],
        ])));
        die;
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
}
